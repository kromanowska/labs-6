﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Contract
{
    public interface IProgram
    {
        void ZacznijGotowanie(string gotuj);
        void ZakonczGotowanie(string zakonczenie);
        void SpalJedzenie(string spal);
        void ZamrozJedzenie(string mrozenie);
    }
}
