﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MExpectedException = Microsoft.VisualStudio.TestTools.UnitTesting.ExpectedExceptionAttribute;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using ExpectedException = NUnit.Framework.ExpectedExceptionAttribute;
using Lab6.Infrastructure;
using PK.Container;
using System.Reflection;
using Lab6.Display.Contract;
using System.Collections.Generic;
using Lab6.ControlPanel.Contract;
using PK.PracaDomowa;

namespace Lab6.Test
{
    [TestFixture, RequiresSTA]
    [TestClass]
    public class P1_Configuration
    {
        [Test]
        [TestMethod]
        public void P1__ContainerFactory_Should_Return_IContainer_Implementation()
        {
            // Arrange
            var container = LabDescriptor.ContainerFactory();

            // Assert
            Assert.That(container, Is.Not.Null);
            Assert.That(container, Is.InstanceOf<IContainer>());
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Implementation_Of_MainComponent_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.MainComponentSpec;
            var component = LabDescriptor.MainComponentImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P1__Contract_And_Implementation_Of_ControlPanel_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.ControlPanelSpec;
            var component = LabDescriptor.ControlPanelImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Implementation_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo(LabDescriptor.MainComponentSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(Assembly.GetAssembly(typeof(IDisplay)).GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P1__ControlPanel_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P1__ControlPanel_Implementation_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.ControlPanelImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework")
                                        .Or.Property("Name").EqualTo(LabDescriptor.ControlPanelSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(LabDescriptor.MainComponentSpec.GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var types = assembly.GetTypes();
            var i = types[0].IsInterface;

            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum);
        }

        [Test]
        [TestMethod]
        public void P1__ControlPanel_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var types = assembly.GetTypes();
            var i = types[0].IsInterface;

            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum);
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Implementation_Library_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.MainComponentSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        public void P1__ControlPanel_Implementation_Library_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.ControlPanelImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.ControlPanelSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Should_Depend_On_Display_Component()
        {
            // Arrange
            var container = LabDescriptor.ContainerFactory();
            var iface = LabDescriptor.MainComponentSpec.GetTypes()[0];
            object result = null;

            // Act
            container.Register(LabDescriptor.MainComponentImpl);
            try
            {
                result = container.Resolve(iface);
            }
            catch (UnresolvedDependenciesException ex) { }

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        [TestMethod]
        public void P1__MainComponent_Should_Be_Automatically_Versioned()
        {
            // Arrange
            var version = LabDescriptor.MainComponentImpl.GetName().Version;
            var build = (int)(DateTime.Now - new DateTime(2000,1,1)).TotalDays;
            var rev = (int)DateTime.Now.TimeOfDay.TotalSeconds / 2;

            // Assert
            Assert.That(version.Build, Is.EqualTo(build));
            Assert.That(version.Revision, Is.GreaterThanOrEqualTo(rev/2).
                                         And.LessThanOrEqualTo(rev));
        }

        [Test]
        [TestMethod]
        public void P1__ControlPanel_Should_Depend_On_MainComponent()
        {
            // Arrange
            var container = LabDescriptor.ContainerFactory();
            object result = null;

            // Act
            container.Register(Assembly.GetAssembly(typeof(IDisplay)));
            container.Register(LabDescriptor.ControlPanelImpl);
            try
            {
                result = container.Resolve<IControlPanel>();
            }
            catch (UnresolvedDependenciesException ex) { }

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        [TestMethod]
        public void P1__Container_Should_Be_Properly_Configured_In_ConfigureApp()
        {
            // Arrange
            var container = Configuration.ConfigureApp();
            
            // Act
            var result = container.Resolve<IControlPanel>();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Window, Is.Not.Null);
        }
    }
}
