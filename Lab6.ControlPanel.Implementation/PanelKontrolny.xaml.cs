﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for PanelKontrolny.xaml
    /// </summary>
     partial class PanelKontrolny : Window
    {
         IProgram program;

        public PanelKontrolny(IProgram program)
        {
            InitializeComponent();
            this.program = program;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            program.ZacznijGotowanie("Gotujeeemy!");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            program.SpalJedzenie("Paaaali się!");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            program.ZakonczGotowanie("Koooniec!");
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            program.ZamrozJedzenie("Brrrr zimnooo!!");
        }
    }
}
