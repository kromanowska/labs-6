﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
   public class ControlPanel1:IControlPanel
    {
       IProgram program;
       Window window;

       public Window Window
       {
           get { return window; }
       }

       public ControlPanel1(IProgram program)
       {
           this.program = program;
           window = new PanelKontrolny(program);
       }
    }
}
