﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;


namespace Lab6.MainComponent.Implementation
{
    public class Mikrofala : IProgram
    {
        IDisplay display;

        public Mikrofala(IDisplay display)
        {
            this.display = display;
        }
        void IProgram.ZacznijGotowanie(string gotuj)
        {
            display.Text = "Gotujemyyyyy ";
        }
        void IProgram.ZakonczGotowanie(string zakonczenie)
        {
            display.Text = "Skonczylo sie :(";
        }
        void IProgram.SpalJedzenie(string spal)
        {
            display.Text = "Spaloneeee!!!!";
        }
        void IProgram.ZamrozJedzenie(string mrozenie)
        {
            display.Text = "brrrrr zamrozone";
        }
    }
}
