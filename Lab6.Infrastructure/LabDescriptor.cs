﻿using Lab6.ControlPanel.Contract;
using Lab6.ControlPanel.Implementation;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;
using PK.Container;
using System;
using System.Reflection;
using PK.PracaDomowa;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel1));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IProgram));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Mikrofala));

        #endregion
    }
}
